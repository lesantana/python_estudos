def printVarInfo(arg1, *vartuple):

    #imprimir um unico parametro
    print('Meu parametro %s' % (arg1))

    #imprimir varios parametros
    for item in vartuple:
        print('Meu parametro : %s' %(item))
    return;

#lambda
Par = lambda x: x % 2 == 0
first = lambda s: s[0]
reverso = lambda s: s[::-1]
addNum = lambda num1, num2: num1 * num2

print(printVarInfo(4,6))

'string'.u
