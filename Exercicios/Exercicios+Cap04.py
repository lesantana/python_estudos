
# coding: utf-8

# In[5]:

# Exercício 2 - Crie uam função que receba uma string como argumento e retorne a mesma string em letras maiúsculas.
# Faça uma chamada à função, passando como parâmetro uma string
def imprimirString(string):
    print(string.upper())
    
imprimirString('leandro')    


# In[2]:

# Exercício 1 - Crie uma função que imprima a sequência de números pares entre 1 e 20 (a função não recebe parâmetro) e 
# depois faça uma chamada à função para listar os números

def imprimir():
    for item in range(2,21,2):
        print(item)

imprimir()


# In[10]:

# Exercício 3 - Crie uma função que receba como parâmetro uma lista de 4 elementos, adicione 2 elementos a lista e 
# imprima a lista
def parametrosEnviados(arg1,arg2,arg3,arg4,*varArgs):
    print('Argumento %s' %(arg1))
    print('Argumento %s' %(arg2))
    print('Argumento %s' %(arg3))
    print('Argumento %s' %(arg4))
    
    for item in varArgs:
        print('Argumento %s' %(item))
    return;
    
parametrosEnviados(1,2,3,4,5,6,7,8,9,10)    


# In[11]:

# Exercício 5 - Crie uma função anônima e atribua seu retorno a uma variável chamada soma. A expressão vai receber 2 
# números como parâmetro e retornar a soma deles

soma = lambda num1, num2: num1+ num2
print(soma(5,10))


# In[ ]:

# Exercício 6 - Execute o código abaixo e certifique-se que compreende a diferença entre variável global e local


# In[ ]:



