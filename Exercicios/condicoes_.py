
# coding: utf-8

# In[7]:

b = 10
print(b)


# In[6]:

nome1, nome2, nome3 = 'Leandro', 'Leonardo', 'Leticia'
print(nome1,nome2,nome3)


# In[8]:

a = 10
b = 20
if b > 20:
    print('n')
elif a == 20:
    print('s')
else:
    print('n/a')


# In[12]:

items = ['banana','maça','abacate','laranja']
for item in items:
    print(item)
    
print('---------------') 

for (i, item) in enumerate(items):
       print(i, item)
    


# In[15]:

continentes = {'europe': 'Europa','american':'America','asia':'Asia'}

for continente in continentes.values():
    print(continente)
    
print('----------------------------')

for (slug, title) in continentes.items():
    print (slug, title)
    
    


# In[30]:

nome = 'Leandro\nSantana'
#print(nome)
#nome[8]
#print(nome[1:])
print(nome[:-1])
nome[::2]


# In[34]:

b = nome + ' Testando o Pynthon'
print(b)
letra = 'w'
letra * 3


# In[45]:

#print(b.upper())
#print(b.lower())
array = b.split()
array_2 = b.split('o');
print(array_2)


# In[59]:

encontrado = nome.find('Santana')
if(encontrado != -1):
    print('encontrado na posicao', encontrado)
else:
    print('nada encontrado parceiro')

