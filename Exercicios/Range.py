
# coding: utf-8

# In[1]:

for i in range(50,101,2):
    print(i)


# In[2]:

for i in range(0,-20,-2):
    print(i)


# In[3]:

temperatura = 40  
while temperatura > 35: 
    print(temperatura)
    temperatura = temperatura - 1    


# In[4]:

listaB = [32,53,85,10,15,17,19]
soma = 0
for i in listaB:
    double_i = i * 2
    soma += double_i

print(soma)

