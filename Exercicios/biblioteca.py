# cria o nome para os convites
def gera_nome_convite(convite):
    return convite[0:4].upper() + '-' + convite[len(convite) - 4:].upper()


# envia os convites
def envia_convite(nome_formato):
    print('Enviando o convite para ' + nome_formato + '...a')


def processa_convite(nome_convidado):
    nome_formato = gera_nome_convite(nome_convidado)
    envia_convite(nome_formato)



