# coding: utf-8
import os

dir = os.getcwd()
dir_leitura = os.path.join(dir, 'pasta_arquivos_leitura')

if os.path.isdir(dir_leitura) == False:
    os.mkdir(dir_leitura)

amount_files = 0
for dir_name, sub_dir, file_list in os.walk(dir_leitura, topdown=False):
    for file in file_list:
        file_path = os.path.join(dir_leitura, file)
        amount_files += 1
        with open(file_path, "r") as file_opened:
            lines = len(file_opened.readlines())
            file_opened.seek(0, 0)
            content = file_opened.read().strip().split()
            len_chars = sum(len(word) for word in content)
            print ('Arquivo[%s]: %s [caracteres: %s] [linhas: %s]' % (amount_files, file, len_chars, lines))

            # limpando variaveis
            file_opened.close()
            del lines
            del content
            del len_chars

print('A pasta contém um total de %s arquivos' % (amount_files))
