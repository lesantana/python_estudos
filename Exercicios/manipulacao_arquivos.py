
# coding: utf-8

# In[36]:

arq1 = open("arquivo_dsc.txt","r");


# In[37]:

print(arq1.read())


# In[38]:

print(arq1.tell())


# In[40]:

print(arq1.seek(0,0))


# In[41]:

print(arq1.read(10))


# In[52]:

arq2 = open("arquivo_dsc.txt","w")


# In[53]:

arq2.write("Testando a gravação de texto no arquivo")
arq2.close()


# In[54]:

arq2 = open("arquivo_dsc.txt","r")
print(arq2.read())


# In[55]:

arq3 = open("arquivo_dsc.txt","a")
arq3.write(' Adicionando mais conteudo ao arquivo')


# In[56]:

arq3 = open("arquivo_dsc.txt","r")
print(arq3.read())


# In[58]:

fileName = input('Digite o nome do arquivo: ')


# In[59]:

fileName = fileName+'.txt'


# In[61]:

arq3 = open(fileName,'w')


# In[62]:

arq3.write('Incluindo texto no arquivo criado')


# In[63]:

arq3.close()


# In[64]:

arq3 = open(fileName,'r')


# In[65]:

arq3.read()


# In[67]:

f = open('salario.csv','r')
data = f.read() 


# In[68]:

rows = data.split('\n')


# In[70]:

print(rows)


# In[72]:

f = open('salario.csv','r')
data = f.read() 
rows = data.split('\n')
full_data = []


# In[73]:

for row in rows:
    split_rows = row.split(';')
    full_data.append(split_rows)


# In[74]:

print(full_data)


# In[76]:

count = 0
for row in full_data:
    count += 1
print(count)    


# In[81]:

f = open('salario.csv','r')
data = f.read() 
rows = data.split('\n')
full_data = []

for row in rows:
    split_row = row.split(';')
    full_data.append(split_row)
    first_row = full_data[0]
count = 0    

for column in first_row:
    count += 1
print(count)    
    


# In[82]:

get_ipython().run_cell_magic('writefile', 'teste.txt', 'Olá este arquivo foi gerado pelo próprio jupiter.\nPodemos gerar quantas linhas quisermos e o Jupiter gera o arquivo final')


# In[90]:

arq4 = open('teste.txt','r')
arq4.read()


# In[87]:

arq4.seek(0,0)


# In[88]:

arq4.readlines()


# In[91]:

for line in open('teste.txt'):
    print(line)


# In[104]:

import pandas as pd
file_name = 'binary.csv'
df = pd.read_csv(file_name)
df.head()


# In[109]:

df2 = pd.read_csv('salario.csv')
df2.head(50)

