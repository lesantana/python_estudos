nome = 'Leandro Santana'
idade = 33
# print('Convite do %s idade %s anos' % (nome,idade))

letras = 'a e i z u'
# print(letras[4:5])
# print(letras[6:9])

convites = ['Romulo', 'Almeida', 'Bronson', 'Nicodemos', 'Rubi']
# print(convites[2:4])

variados = ['A', 'B', 12, 'F', 14]
variados.remove('B')
# print(variados)

# tuplas
tipos_convite = ('vip', 'normal', 'meia', 'cortesia')
# listas
tipos_convite2 = ['vip', 'normal', 'meia', 'cortesia']
# dicionario
convite_com_valor = {'vip': 60, 'normal': 40, 'meia': 30, 'cortesia': 0}
# print(convite_com_valor['vip'])

estados = ('RJ', 'SP') + tuple(['MG', 'ES'])
# print(estados)

nomes = ('Leandro', 'Leonardo', 'Amanda', 'Joana', 'Isac')
# print(sorted(nomes))

materias_com_peso = {'Equações Diofantinas': 3, 'Álgebra Relacional': 2, 'Português instrumental': 4}
pesos = tuple(materias_com_peso.values())

# python/biblioteca.py
from biblioteca import *
processa_convite('Leandro Santana')

import os
file = (os.getcwd())
print(file)
#arq1 = open(file,"r");
