# coding: utf-8

# Classe Bola: Crie uma classe que modele uma bola:
# Atributos: Cor, circunferência, material
# Métodos: trocaCor e mostraCor
def exercicio_1():
    from MyClass import Bola
    bola01 = Bola("verde", 5, "metal")
    while True:
        print('A cor atual da bola é %s' % bola01.mostraCor())
        continuar = raw_input("Deseja Continuar? [s/n]: ")
        if continuar.upper() == 'N':
            break
        else:
            bola01.trocaCor()


# Classe Quadrado: Crie uma classe que modele um quadrado:
# Atributos: Tamanho do lado
# Métodos: Mudar valor do Lado, Retornar valor do Lado e calcular Área;
def exercicio_2():
    from MyClass import Quadrado
    quadradoClass = Quadrado(6)
    while True:
        resultado = quadradoClass.mostraValorLado()
        print('O valor do lado do quadrado é %s cm e sua área total é %s cm' % (resultado['tamanho_lado'], resultado['tamanho_area']))
        continuar = raw_input("Deseja Continuar? [s/n]: ")
        if continuar.upper() == 'N':
            break
        else:
            quadradoClass.trocaAreaQuadrado()
    del resultado

# Classe Retangulo: Crie uma classe que modele um retangulo:
# Atributos: LadoA, LadoB (ou Comprimento e Largura, ou Base e Altura, a escolher)
# Métodos: Mudar valor dos lados, Retornar valor dos lados, calcular Área e calcular Perímetro;
# Crie um programa que utilize esta classe. Ele deve pedir ao usuário que informe as medidades de um local. Depois, deve criar um objeto com as medidas e calcular a quantidade de pisos e de rodapés necessárias para o local.
def exercicio_3():
   from MyClass import Retangulo

   retangulo = Retangulo()
   retangulo.mudarValorLados()

   valor = retangulo.retornaValorLados()
   print(valor)

   area = retangulo.calcularArea()
   print(area)

   perimetro = retangulo.calcularPerimetro()
   print(perimetro)



# exercicio_1()
# exercicio_2()
exercicio_3()
