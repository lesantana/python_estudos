# coding: utf-8
# !/usr/bin/python
# MyClass.py
class Bola(object):
    def __init__(self, cor='', circunferencia=0, material=''):
        self.cor = cor
        self.circunferencia = circunferencia
        self.material = material

    def trocaCor(self):
        try:
            troca = raw_input("Deseja mudar a cor atual {}? [s/n]: ".format(self.cor))
            if troca.upper() not in ['S', 'N']:
                raise Exception('opção inválida, tente novamente.')
            elif troca.upper() == 'S':
                nova_cor = raw_input('Informe a nova cor: ')
                self.cor = nova_cor

        except Exception as err:
            print('Erro: %s' % err)

    def mostraCor(self):
        try:
            return format(self.cor)
        except Exception as err:
            print('Erro: %s' % err)


class Quadrado(object):
    def __init__(self, tamanho_lado=0):
        self.tamanho_lado = tamanho_lado
        self.area_quadrado = 0

    def trocaAreaQuadrado(self):
        try:
            troca = raw_input("Deseja troca o tamanho do lado do quadrado {}? [s/n]: ".format(self.tamanho_lado))
            if troca.upper() not in ['S', 'N']:
                raise Exception('opção inválida, tente novamente.')
            elif troca.upper() == 'S':
                novo_tamanho = input('Informe o novo tamanho do lado: ')
                self.tamanho_lado = novo_tamanho
        except Exception as err:
            print('Erro: %s' % err)

    def mostraValorLado(self):
        try:
            self.area_quadrado = self.tamanho_lado ** 2
            return {'tamanho_area': self.area_quadrado, 'tamanho_lado': self.tamanho_lado}
        except Exception as err:
            print('Erro: %s' % err)


class Retangulo(object):
    def __init__(self, Base=0, Altura=0):
        self.Base = Base
        self.Altura = Altura
        self.Area = 0
        self.Perimetro = 0

    def retornaValorLados(self):
        return {'base': self.Base, 'altura': self.Altura}

    def mudarValorLados(self):
        self.Base = int(input('Informe o valor da base: '))
        self.Altura = int(input('Informe o valor da altura: '))

    def calcularArea(self):
        self.Area = self.Base * self.Altura
        return self.Area

    def calcularPerimetro(self):
        self.Perimetro = 2 * (self.Base + self.Altura)
        return self.Perimetro
