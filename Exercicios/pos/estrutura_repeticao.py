# coding: utf-8
# !/usr/bin/python
import sys
import numbers


# Faça um programa que peça uma nota, entre zero e dez.
# Mostre uma mensagem caso o valor seja inválido e
# continue pedindo até que o usuário informe um valor válido.
def exercicio_1():
    try:
        numero = input("Digite um numero de 0 a 10 :  ")
        if type(numero) is not int:
            print('Digite apenas números inteiros')

        while 0 > numero or 10 < numero:
            numero = input("Digite um numero de 0 a 10 : ")

    except Exception as err:
        print('mensagem: %s' % err)
        exercicio_1()


# Faça um programa que leia um nome de usuário e a sua senha e não aceite a senha igual ao nome do usuário,
# mostrando uma mensagem de erro e voltando a pedir as informações.
def exercicio_2():
    try:
        nome_usuario = raw_input("Informe o nome do usuário: ")
        senha_usuario = raw_input("Informe a senha do usuário:  ")

        while (nome_usuario == senha_usuario):
            print('Erro: _Não é possível informar usuário e senha iguais_ Tente Novamente')
            nome_usuario = raw_input("Informe o nome do usuário: ")
            senha_usuario = raw_input("Informe a senha do usuário:  ")

    except Exception as err:
        print('mensagem: %s' % err)


# Faça um programa que leia e valide as seguintes informações:
# Nome: maior que 3 caracteres;
# Idade: entre 0 e 150;
# Salário: maior que zero;
# Sexo: 'f' ou 'm';
# Estado Civil: 's', 'c', 'v', 'd';

def exercicio_3():
    try:
        nome = raw_input("Informe o nome: ")
        if len(nome) <= 3:
            raise Exception("O nome deve ter mais de 3 caracteres.")

        idade = int(input("Informe a idade:  "))
        if idade not in range(0, 151):
            raise Exception("A idade deve estar entre 0 e 150")

        salario = float(input("Informe o salario:  "))
        if salario == 0:
            raise Exception("O Salario deve ser maior que zero")

        sexo = raw_input("Informe o sexo:  ")
        if sexo.upper() not in ['F', 'M']:
            raise Exception("O sexo deve ser Masculino(m) ou Feminino(f)")

        estado_civil = raw_input("Informe o estado civil:  ")
        if estado_civil.upper() not in ['S', 'C', 'V', 'D']:
            raise Exception("O estado civil deve ser S[Solteiro], C[Casado], V[Viúvo], D[Divociado]")

    except Exception as err:
        print('erro: %s' % err)
        exercicio_3()


# Supondo que a população de um país A seja da ordem de 80000
# habitantes com uma taxa anual de crescimento de 3% e que a
# população de B seja 200000 habitantes com uma taxa de crescimento de 1.5%.
# Faça um programa que calcule e escreva o número de anos
# necessários para que a população do país A ultrapasse ou
# iguale a população do país B, mantidas as taxas de crescimento.
def exercicio_4():
    try:
        populacaoPaisA = 80.000
        taxaCrescimentoPaisA = 0.03

        populacaoPaisB = 200.000
        taxaCrescimentoPaisB = 0.015

        list = [0]
        for item in list:
            populacaoPaisA += populacaoPaisA * taxaCrescimentoPaisA
            populacaoPaisB += populacaoPaisB * taxaCrescimentoPaisB
            list.append(item + 1)
            if populacaoPaisA > populacaoPaisB:
                break
        print('População pais A %s ' % populacaoPaisA)
        print('População pais B %s ' % populacaoPaisB)
        print('O Total de anos necessários para o pais A \n' \
              'Alcançar a população do pais B será de %s anos' % list[-1])

    except Exception as err:
        print('erro: %s' % err)


# Altere o programa anterior permitindo ao usuário informar as populações e as taxas de crescimento iniciais.
# Valide a entrada e permita repetir a operação.
def exercicio_5():
    while True:
        try:
            populacaoPaisA = input('Informe a população do pais A: ')
            if type(populacaoPaisA) is not int and type(populacaoPaisA) is not float:
                raise Exception('você deve informar números')
            taxaCrescimentoPaisA = input('Informe a taxa de crescimento da população do pais A: ')
            if type(taxaCrescimentoPaisA) is not int and type(taxaCrescimentoPaisA) is not float:
                raise Exception('você deve informar números')
            populacaoPaisB = input('Informe a população do pais B: ')
            if type(populacaoPaisB) is not int and type(populacaoPaisB) is not float:
                raise Exception('você deve informar números')
            taxaCrescimentoPaisB = input('Informe a taxa de crescimento da população do pais A: ')
            if type(taxaCrescimentoPaisB) is not int and type(taxaCrescimentoPaisB) is not float:
                raise Exception('você deve informar números')
        except Exception as err:
            print('erro: %s' % err)
            continue

        else:
            list = [0]
            taxaCrescimentoPaisA = float(float(taxaCrescimentoPaisA) / 100)
            taxaCrescimentoPaisB = float(float(taxaCrescimentoPaisB) / 100)
            for item in list:
                populacaoPaisA += populacaoPaisA * taxaCrescimentoPaisA
                populacaoPaisB += populacaoPaisB * taxaCrescimentoPaisB
                list.append(item + 1)
                if populacaoPaisA > populacaoPaisB:
                    break
            print('População pais A %s ' % populacaoPaisA)
            print('População pais B %s ' % populacaoPaisB)
            print('O Total de anos necessários para o pais A \n' \
                  'Alcançar a população do pais B será de %s anos' % list[-1])
            break


# Faça um programa que imprima na tela os números de 1 a 20, um abaixo do outro.
# Depois modifique o programa para que ele mostre os números um ao lado do outro.
def exercicio_6():
    for i in range(1, 21):
        print(i)
    print(range(1, 21))


# Faça um programa que leia 5 números e informe o maior número.
def exercicio_7():
    while True:
        try:
            n_1 = input('Informe o primeiro número: ')
            n_2 = input('Informe o segundo número: ')
            n_3 = input('Informe o terceiro número: ')
            n_4 = input('Informe o quarto número: ')
            n_5 = input('Informe o quinto número: ')

            if type(n_1) is not int:
                raise Exception('O primeiro número deve ser inteiro')
            elif type(n_2) is not int:
                raise Exception('O segundo número deve ser inteiro')
            elif type(n_3) is not int:
                raise Exception('O terceiro número deve ser inteiro')
            elif type(n_4) is not int:
                raise Exception('O quarto número deve ser inteiro')
            elif type(n_5) is not int:
                raise Exception('O quinto número deve ser inteiro')

        except Exception as err:
            print('erro: %s' % err)
            print('_Programa Reiniciado_\n')
            continue
        else:
            lst_n = [n_1, n_2, n_3, n_4, n_5]
            print('O maior número informado é %s ' % max(lst_n))
            break


# Faça um programa que leia 5 números e informe a soma e a média dos números.
def exercicio_8():
    while True:
        try:
            n_1 = input('Informe o primeiro número: ')
            n_2 = input('Informe o segundo número: ')
            n_3 = input('Informe o terceiro número: ')
            n_4 = input('Informe o quarto número: ')
            n_5 = input('Informe o quinto número: ')

            if type(n_1) is not int:
                raise Exception('O primeiro número deve ser inteiro')
            elif type(n_2) is not int:
                raise Exception('O segundo número deve ser inteiro')
            elif type(n_3) is not int:
                raise Exception('O terceiro número deve ser inteiro')
            elif type(n_4) is not int:
                raise Exception('O quarto número deve ser inteiro')
            elif type(n_5) is not int:
                raise Exception('O quinto número deve ser inteiro')

        except Exception as err:
            print('erro: %s' % err)
            print('_Programa Reiniciado_\n')
            continue
        else:
            lst_n = [n_1, n_2, n_3, n_4, n_5]
            print('A soma dos números é %s ' % sum(lst_n))
            print('A média dos números é %s ' % float(sum(lst_n) / int(len(lst_n))))
            break


# Faça um programa que imprima na tela apenas os números ímpares entre 1 e 50.
def exercicio_9():
    lst_numero = range(1, 51)
    lst_impares = []
    lst_pares = []
    for n in lst_numero:
        if n % 2 != 0:
            lst_impares.append(n)
        else:
            lst_pares.append(n)
    print('_Número Impares_')
    print(lst_impares)
    print('_Número Pares_')
    print(lst_pares)


# Faça um programa que receba dois números inteiros e gere os números inteiros que estão no intervalo
# compreendido por eles.
def exercicio_10():
    while True:
        try:
            n_1 = input('Informe o primeiro número: ')
            n_2 = input('Informe o segundo número: ')

            if type(n_1) is not int:
                raise Exception('O primeiro número deve ser inteiro')
            elif type(n_2) is not int:
                raise Exception('O segundo número deve ser inteiro')

        except Exception as err:
            print('erro: %s' % err)
            print('_Programa Reiniciado_\n')
            continue
        else:
            lst_n = range(n_1 + 1, n_2)
            if len(lst_n) > 0:
                print('Os números no intervalo de %s entre %s' % (n_1, n_2))
                print(lst_n)
            else:
                print('Não existe números no intervalo de %s entre %s' % (n_1, n_2))
        break
