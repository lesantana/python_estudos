# coding: utf-8
import math
from numbers import Number


# Faça um Programa que peça dois números e imprima o maior deles.
def exercicio_1():
    try:
        numero_1 = input('Informe o primeiro numero: ')
        numero_2 = input('Informe o segundo numero: ')
        if type(numero_1) is not int or type(numero_2) is not int:
            raise Exception('você deve informar números inteiros')

        if numero_1 > numero_2:
            print('O maior número é %s' % numero_1)
        elif (numero_2 > numero_1):
            print('O maior número é %s' % numero_2)
        else:
            print('Os números são iguais')
    except Exception as err:
        print('O programa será finalizado %s' % err)


# Faça um Programa que peça um valor e mostre na tela se o valor é positivo ou negativo.
def exercicio_2():
    try:
        numero = input('Informe o primeiro numero: ')
        if type(numero) is not int:
            raise Exception('você deve informar números inteiros')

        if numero < 0:
            print('O número é negativo')
        else:
            print('O número é positivo')

    except Exception as err:
        print('O programa será finalizado %s' % err)


# Faça um Programa que verifique se uma letra digitada é &quot;F&quot; ou &quot;M&quot;. Conforme a letra escrever: F
# - Feminino, M - Masculino, Sexo Inválido.
def exercicio_3():
    try:
        letra = raw_input('Informe a letra para o programa: ')
        if type(letra) is not str:
            raise Exception('Você precisa informar letras')
        if (letra.upper() == 'F'):
            print ('Sexo feminino')
        elif (letra.upper() == 'M'):
            print ('Sexo masculino')
        else:
            print('Sexo invalido')
    except Exception as err:
        print('O programa será finalizado %s' % err)


# Faça um Programa que verifique se uma letra digitada é vogal ou consoante.
def exercicio_4():
    try:
        vogais = ['a', 'e', 'i', 'o', 'u']
        letra = raw_input('Informe a letra para o programa: ')
        if letra in vogais:
            print("Vogal")
        elif letra.isalpha():
            print("Consoante")
        else:
            raise Exception('Você precisa informar letras')
    except Exception as err:
        print('O programa será finalizado %s' % err)


# Faça um programa para a leitura de duas notas parciais de um aluno. O programa deve calcular a média alcançada por aluno e apresentar:
# A mensagem "Aprovado", se a média alcançada for maior ou igual a sete;
# A mensagem "Reprovado", se a média for menor do que sete;
# A mensagem "Aprovado com Distinção", se a média for igual a dez.
def exercicio_5():
    try:
        nota_1 = input('Informe a primeira nota: ')
        nota_2 = input('Informe a segunda nota: ')

        resultado = (nota_1 + nota_2) / 2;

        if resultado >= 7 and resultado < 10:
            print('Aprovado você recebeu nota %s' % resultado)
        elif resultado >= 10:
            print('Aprovado com distinção você recebeu nota %s' % resultado)
        else:
            print('Você foi reprovado recebeu nota %s' % resultado)
    except Exception as err:
        print('O programa será finalizado %s' % err)


# Faça um Programa que leia três números e mostre o maior deles.
def exercicio_6():
    try:
        n_1 = input('Informe o primeiro número: ')
        n_2 = input('Informe o segundo número: ')
        n_3 = input('Informe o terceiro número: ')

        if n_1 > n_2 and n_1 > n_3:
            print('O maior é o primeiro número informado %s' % n_1)
        elif n_2 > n_1 and n_2 > n_3:
            print('O maior é o segundo número informado %s' % n_2)
        elif n_3 > n_1 and n_3 > n_2:
            print('O maior é o terceiro número informado %s' % n_3)
        else:
            print('Os números são iguais')

    except Exception as err:
        print('O programa será finalizado %s' % err)


# Faça um Programa que leia três números e mostre o maior e o menor deles
def exercicio_7():
    try:
        n_1 = input('Informe o primeiro número: ')
        n_2 = input('Informe o segundo número: ')
        n_3 = input('Informe o terceiro número: ')
        iguais = 0

        # verifica maior
        if n_1 > n_2 and n_1 > n_3:
            print('O maior é o primeiro número informado %s' % n_1)
        elif n_2 > n_1 and n_2 > n_3:
            print('O maior é o segundo número informado %s' % n_2)
        elif n_3 > n_1 and n_3 > n_2:
            print('O maior é o terceiro número informado %s' % n_3)
        else:
            iguais = 1

        # verifica o menor
        if n_1 < n_2 and n_1 < n_3:
            print('O menor é o primeiro número informado %s' % n_1)
        elif n_2 < n_1 and n_2 < n_3:
            print('O menor é o segundo número informado %s' % n_2)
        elif n_3 < n_1 and n_3 < n_2:
            print('O menor é o terceiro número informado %s' % n_3)
        else:
            iguais = 1

        if iguais == 1:
            print('Os números são iguais')

    except Exception as err:
        print('O programa será finalizado %s' % err)


# Faça um programa que pergunte o preço de três produtos e
# informe qual produto você deve comprar,
# sabendo que a decisão é sempre pelo mais barato.
def exercicio_8():
    try:
        prod_1 = float(input('Informe o preço do primeiro produto: '))
        prod_2 = float(input('Informe o preço do segundo produto: '))
        prod_3 = float(input('Informe o preço do terceiro produto: '))

        if prod_1 < prod_2 and prod_1 < prod_3:
            print('Você deve comprar o primeiro produto o preço é R$ %s' % prod_1)
        elif prod_2 < prod_1 and prod_2 < prod_3:
            print('Você deve comprar o segundo produto o preço é R$ %s' % prod_2)
        elif prod_3 < prod_1 and prod_3 < prod_2:
            print('Você deve comprar o terceiro produto o preço é R$ %s' % prod_3)
        else:
            print('Ficou dificil o preço dos produtos é igual R$ %s' % prod_3)

    except Exception as err:
        print('O programa será finalizado %s' % err)


# Faça um Programa que leia três números e mostre-os em ordem decrescente
def exercicio_9():
    try:
        n_1 = raw_input('Informe o primeiro número: ')
        n_2 = raw_input('Informe o segundo número: ')
        n_3 = raw_input('Informe o terceiro número: ')
        aList = []
        aList.append(n_1)
        aList.append(n_2)
        aList.append(n_3)
        aList.sort(reverse=False)
        for n in aList:
            print('Numero %s' % n)

    except Exception as err:
        print('O programa será finalizado %s' % err)

#Faça um Programa que pergunte em que turno você estuda.
#Peça para digitar M-matutino ou V-Vespertino ou N- Noturno.
#Imprima a mensagem "Bom Dia!", "Boa Tarde!" ou "Boa Noite!" ou "Valor Inválido!",
#conforme o caso.
def exercicio_10():
    try:
        print('::..Programa Informe o Turno do Estudante..::')
        print('::..Para o turno Matutino digite "M"..::\n'\
              '::..Para o turno Vespertino digite "V"..::\n'\
              '::..Para o turno Noturno digite "N"..::\n\n'\
              '::..Não serão aceitos outras siglas..::\n')

        comecar = raw_input('Digite "s" para começar, ou qualquer para finalizar: ')

        if(comecar.upper() == 'S'):
            periodo = raw_input('Informe o periodo: ')

            if periodo.upper() == 'M':
                print('\nBom dia Estudante do periodo Matutino')
            elif periodo.upper() == 'V':
                print('\nBom tarde Estudante do periodo Vespertino')
            elif periodo.upper() == 'N':
                print('\nBom tarde Estudante do periodo Noturno')
            else:
                print('\nO valor informa é invalido.')
        else:
            print('Você finalizou o programa. Obrigado')
    except Exception as err:
        print('O programa será finalizado %s' % err)
