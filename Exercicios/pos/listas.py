#coding: utf-8

#Faça um Programa que leia um vetor de 5 números inteiros e mostre-os.
def exercicio1():
	vet = []
	for i in range(5):
		vet.append(input('Informe o ' + str(i+1) + '.o Valor: '))
	print vet

#Faça um Programa que leia um vetor de 10 números reais e mostre-os na ordem inversa.
def exercicio2():
	vet = []
	for i in range(10):
		vet.append(input('Informe o ' + str(i+1) + '.o Valor: '))
	print vet[::-1]

# Faça um Programa que peça as quatro notas de 10 alunos, calcule e armazene num vetor
# a média de cada aluno, imprima o número de alunos com média maior ou igual a 7.0.
def exercicio3():
    notas = []
    contAluno = 0
    notasMedia = [10]
    contMedia = 0
    cont = 0
    contMedia = 0
    count = 0
    for i in range(40):
        if (i % 4 == 0 and i <> 0):
            contAluno += 1
            print str(contAluno + 1) + ' - Prox. Aluno'
            cont = 1
            notas.append(input('Informe a ' + str(cont) + '.o Nota: '))
            notasMedia.insert(contMedia, ((notas[i - 1] + notas[i - 2] + notas[i - 3] + notas[i - 4]) / 4))
            contMedia += 1
        else:
            cont += 1
            notas.append(input('Informe a ' + str(cont) + '.o Nota: '))
    # print notas

    for w in range(10):
        if (notasMedia[w] >= 7):
            count += 1
    print "Alunos com medias > or = 7: " + str(count)

# Faça um Programa que leia um vetor de 5 números inteiros, mostre a soma, a multiplicação e os números.
def exercicio4():
    vet = []
    for i in range(5):
        vet.append(input('Informe o ' + str(i + 1) + '.o Valor: '))

    multiplicacao = vet[0]
    j = 1
    for j in range(5):
        multiplicacao = vet[j] * multiplicacao

    print 'Soma: ' + str(sum(vet))
    print 'Multiplica: ' + str(multiplicacao)
    print vet


#exercicio1()
#exercicio2()
#exercicio3()
#exercicio4()