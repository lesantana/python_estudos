# coding: utf-8
# !/usr/bin/python

# Tamanho de strings. Faça um programa que leia 2 strings e informe o conteúdo delas seguido do seu comprimento.
# Informe também se as duas strings possuem o mesmo comprimento e são iguais ou diferentes no conteúdo.
def exercicio_1():
    try:
        string_1 = raw_input('Informe A primeira string 1: ')
        string_2 = raw_input('Informe A primeira string 2: ')

        print('A string 1 [%s] possuí [%s] caracteres' % (string_1, len(string_1)))
        print('A string 2 [%s] possuí [%s] caracteres' % (string_2, len(string_2)))

        if (len(string_1) == len(string_2)):
            print('As duas strings possuem o mesmo tamanho.')
        else:
            print('As duas strings não possuem o mesmo tamanho.')
        if (string_1 == string_2):
            print('As duas string são iguais.')
        else:
            print('As duas string não são iguais.')

    except Exception as err:
        print('Erro: %s' % err)


# Nome ao contrário em maiúsculas.
# Faça um programa que permita ao usuário digitar o seu nome e em seguida mostre o
# nome do usuário de trás para frente utilizando somente letras maiúsculas.
# Dica: lembre−se que ao informar o nome o usuário pode digitar letras maiúsculas ou minúsculas.
def exercicio_2():
    try:
        nome = raw_input('Informe seu nome:  ')
        nome_convertido = nome[::-1].upper()
        print('O nome foi convertido em ordem contrária: [%s]' % nome_convertido)
    except Exception as err:
        print('Erro: %s' % err)


# Nome na vertical. Faça um programa que solicite o nome do usuário e imprima-o na vertical.
def exercicio_3():
    try:
        nome = raw_input('Informe seu nome:  ')
        for i in nome:
            print(i.upper())

    except Exception as err:
        print('Erro: %s' % err)


# Nome na vertical em escada.
# Modifique o programa anterior de forma a mostrar o nome em formato de escada.
def exercicio_4():
    try:
        nome = raw_input('Informe seu nome:  ')
        for idx, i in enumerate(nome):
            print(nome[0:idx + 1])
    except Exception as err:
        print('Erro: %s' % err)


# Nome na vertical em escada invertida.
# Altere o programa anterior de modo que a escada seja invertida.
def exercicio_5():
    try:
        nome = raw_input('Informe seu nome:  ')
        for idx, i in enumerate(nome):
            print(nome[0:len(nome) - idx])
    except Exception as err:
        print('Erro: %s' % err)


# Data por extenso. Faça um programa que
# solicite a data de nascimento (dd/mm/aaaa)
# do usuário e imprima a data com o nome do mês por extenso.
def exercicio_6():
    try:
        data = raw_input("digite uma data com o seguinte formato dd/mm/aaaa ---> ")
        dia = int(data[0:2])
        mes = int(data[3:5])
        ano = int(data[6:10])
        validade = "true"
        i = 0
        while validade == "true" and i == 0:
            if (ano % 4 == 0 and ano % 100 != 0) or ano % 400 == 0:
                bissexto = "sim"
            else:
                bissexto = "nao"

            if mes < 1 or mes > 12:
                validade = "false"

            if dia > 31 or ((mes == 4 or mes == 6 or mes == 9 or mes == 11) and dia > 30):
                validade = "false"

            if (mes == 2 and bissexto == "nao" and dia > 28) or (mes == 2 and bissexto == "sim" and dia > 29):
                validade = "false"
            i = i + 1

        if validade == "true":
            lista_de_mes = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto",
                            "Setembro",
                            "Outubro", "Novembro", "Dezembro"]
            mes_ex = lista_de_mes[mes - 1]
            print('Você nasceu em %s de %s de %s' % (dia, mes_ex, ano))
        else:
            raise Exception('Data Inválida')
    except Exception as err:
        print('Erro: %s' % err)


# Conta espaços e vogais. Dado uma string com uma frase informada pelo usuário (incluindo espaços em branco), conte:
# quantos espaços em branco existem na frase.
# quantas vezes aparecem as vogais a, e, i, o, u.
def exercicio_7():
    try:
        vogais = ['a', 'e', 'i', 'o', 'u']
        string = raw_input('Informe a string:  ')
        count_espacos_branco = string.count(' ')
        lst = []
        for v in string:
            if v in vogais:
                lst.append(v)
        print('A quantidde de vogais encontradas é %s' % len(lst))
        del lst
        if(count_espacos_branco > 0):
            print('Foram encontrados um total de %s espaços em branco' % count_espacos_branco)
        else:
            print('Não foram localizados espaços em branco na string')
        del count_espacos_branco
    except Exception as err:
        print('Erro: %s' % err)

#Palíndromo. Um palíndromo é uma seqüência de caracteres cuja leitura é
# idêntica se feita da direita para esquerda ou vice−versa. Por exemplo:
# OSSO e OVO são palíndromos. Em textos mais complexos os espaços e pontuação são
# ignorados. A frase SUBI NO ONIBUS é o exemplo de uma frase palíndroma onde os
# espaços foram ignorados. Faça um programa que leia uma seqüência de caracteres, mostre−a e diga se é um palíndromo ou não.
def exercicio_8():
    try:
        string = raw_input('Informe a string: ')
        stringSemEspacos = string.replace(' ', '')
        stringTodaMinuscula = stringSemEspacos.lower()
        stringInvertida = stringTodaMinuscula[::-1]
        if stringInvertida == stringTodaMinuscula:
            print('Sim é um Palíndromo')
        else:
            print('Não é um Palíndromo')
    except Exception as err:
        print('Erro: %s' % err)

#Verificação de CPF.
# Desenvolva um programa que solicite a digitação de um número de CPF no formato xxx.xxx.xxx-xx e indique se é um número válido ou inválido através da validação dos dígitos verificadores edos caracteres de formatação.
def exercicio_9():

def exercicio_10():

# exercicio_1()
# exercicio_2()
# exercicio_3()
# exercicio_4()
# exercicio_6()
# exercicio_7()
# exercicio_8()

