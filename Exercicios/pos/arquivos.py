# Faça um programa que leia um arquivo texto contendo uma lista de endereços IP e gere um
# outro arquivo, contendo um relatório dos endereços IP válidos e inválidos.O arquivo de
# entrada possui o seguinte formato:

def validaIP(ipAddress):
    blocos = ipAddress.split('.')
    if (len(blocos) != 4):
        return False
    for i in blocos:
        i_i = int(i)
        if (i_i < 0) or (i_i > 255):
            return False
    return True

arquivoEntrada = open('c:\\entrada.txt', 'r')
linhas = arquivoEntrada.readlines()
arquivoEntrada.close()
ipsValidos = []
ipsInvalidos = []
for ip in linhas:
    if (validaIP(ip.strip())):
        ipsValidos.append(ip.strip())
    else:
        ipsInvalidos.append(ip.strip())
arquivoSaida = open('c:\\saida.txt', 'w')
arquivoSaida.write('[IPs Validos]\n')
for ip in ipsValidos:
    arquivoSaida.write('%s\n' % ip)

arquivoSaida.write('\n[IPs Invalidos]\n')
for ip in ipsInvalidos:
    arquivoSaida.write('%s\n' % ip)
arquivoSaida.close()
arquivoSaida = open('c:\\saida.txt', 'r')
print (arquivoSaida.read())
