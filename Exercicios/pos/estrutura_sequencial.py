# coding: utf-8
import math
from numbers import Number


# Faça um Programa que mostre a mensagem &quot;Alo mundo&quot; na tela.
def exercicio_1():
    print('Olá Mundo')


# Faça um Programa que peça um número e então mostre a mensagem O número informado foi
def exercicio_2():
    x = input('Informe um número:')
    print(x)


# Faça um Programa que peça dois números e imprima a soma.
def exercicio_3():
    x = input('Informe o primeiro número')
    y = input('Informe o segundo número')
    print(x + y)


# Faça um Programa que peça as 4 notas bimestrais e mostre a média.
def exercicio_4():
    primeiro = input('Informe a primeira nota')
    segundo = input('Informe o segunda nota')
    terceira = input('Informe o terceira nota')
    quarta = input('Informe o quarta nota')
    media = (primeiro + segundo + terceira + quarta) / 4
    print(media)


# Faça um Programa que converta metros para centímetros.
def exercicio_5():
    primeiro = input('Informe a metragem:')
    print (primeiro * 100)


# Faça um Programa que peça o raio de um círculo, calcule e mostre sua área.
def exercicio_6():
    primeiro = input('Informe o raio do circulo')
    a = 3.14 * math.pow(primeiro, 2)
    print(a)


# Faça um Programa que calcule a área de um quadrado, em seguida mostre o dobro desta área para o usuário.
def exercicio_7():
    area = input('Informe o tamanho de um lado do quadrado')
    area = area * area
    print(area * 2)


# Faça um Programa que pergunte quanto você ganha por hora e o número de horas trabalhadas
# no mês. Calcule e mostre o total do seu salário no referido mês.
def exercicio_8():
    ganho = input('Quanto você ganha por hora')
    numero_horas = input('Informe o número de horas trabalhadas')
    ganho_1 = ganho * numero_horas
    print('Eu ganho %s por hora' % ganho_1)


# Faça um Programa que peça a temperatura em graus Farenheit, transforme e mostre a
# temperatura em graus Celsius.
def exercicio_9():
    temp = input('Informe a temperatura Farenheit:')
    celsius = (temp - 32) / 1.8
    print(celsius)


# Faça um Programa que peça a temperatura em graus Celsius, transforme e mostre em graus
# Farenheit.
def exercicio_10():
    temp = input('Informe a temperatura Celsius:')
    fahrenheit = (temp * 1.8) + 32
    print(fahrenheit)


# 11.Faça um Programa que peça 2 números inteiros e um número real. Calcule e mostre:
# a.	o produto do dobro do primeiro com metade do segundo .
# b.	a soma do triplo do primeiro com o terceiro.
# c.	o terceiro elevado ao cubo.
def exercicio_11():
    try:
        n_inteiro1 = input('Informe o primeiro número inteiro: ')
        if type(n_inteiro1) is not int:
            raise Exception("O primeiro número não é inteiro")

        n_inteiro2 = input('Informe o segundo número inteiro: ')
        if type(n_inteiro2) is not int:
            raise Exception("O segundo número não é inteiro")

        n_real = input('Informe o terceiro número do tipo real:')
        if type(n_real) is not float:
            raise Exception("O terceiro número não é real")

        # inicia a)
        quest_a = float(n_inteiro1 * 2) * float(n_inteiro2 / 2)
        print('O produto do dobro do primeiro com metade do segundo: %s' % float(quest_a))

        quest_b = (n_inteiro1 * 3) + n_real
        print('A soma do triplo do primeiro com o terceiro: %s' % float(quest_b))

        quest_c = math.pow(n_real, 3)
        print('O terceiro elevado ao cubo: %s' % float(quest_c))
    except Exception as err:
        print('O programa será finalizado, %s' % err)


# Tendo como dados de entrada a altura de uma pessoa, construa um algoritmo que calcule seu
# peso ideal, usando a seguinte fórmula: (72.7*altura) - 58
def exercicio_12():
    try:
        v_altura = input('Informe sua altura: ')
        if type(v_altura) is not float:
            raise Exception("É necessário informar a sua altura no formato correto [ex(1.70)]")
        v_sexo = input('Informe seu sexo 1[masculino] 2[feminino]: ')
        if (v_sexo not in [1, 2]):
            raise Exception("Você precisa informar o sexo entre as opções 1 ou 2")
        resultado = 0
        if v_sexo == 1:
            resultado = 80 - (((v_altura * 100) - 150) / 4)
            print('Homem seu peso ideal é: %s' % resultado)
        elif v_sexo == 2:
            resultado = 70 - (((v_altura * 100) - 150) / 2)
            print('Mulher seu peso ideal é: %s' % resultado)
    except Exception as err:
        print('O programa será finalizado, %s' % err)


# Tendo como dados de entrada a altura e o sexo de uma pessoa, construa um algoritmo que calcule seu peso ideal, utilizando as seguintes fórmulas:
# Para homens: (72.7*h) - 58
# Para mulheres: (62.1*h) - 44.7 (h = altura)
# Peça o peso da pessoa e informe se ela está dentro, acima ou abaixo do peso.
def exercicio_13():
    try:
        v_altura = input('Informe sua altura: ')
        if type(v_altura) is not float:
            raise Exception("É necessário informar a sua altura no formato correto [ex(1.70)]")
        v_sexo = input('Informe seu sexo 1[masculino] 2[feminino]: ')
        if (v_sexo not in [1, 2]):
            raise Exception("Você precisa informar o sexo entre as opções 1 ou 2")
        v_peso_atual = input('Informe seu peso atual: ')
        if (not isinstance(v_peso_atual, Number)):
            raise Exception("você precisa informar seu peso de forma correta em números.")
        resultado = 0
        if v_sexo == 1:
            resultado = 80 - (((v_altura * 100) - 150) / 4)
            if (float(resultado) < float(v_peso_atual)):
                print('De acordo com o peso informado você está acima do peso ideal.')
            elif float(resultado) > float(v_peso_atual):
                print('De acordo com o peso informado você está abaixo do peso ideal.')
            print('Se peso ideal é: %s' % resultado)
        elif v_sexo == 2:
            resultado = 70 - (((v_altura * 100) - 150) / 2)
            if (float(resultado) < float(v_peso_atual)):
                print('De acordo com o peso informado você está acima do peso ideal.')
            elif float(resultado) > float(v_peso_atual):
                print('De acordo com o peso informado você está abaixo do peso ideal.')
            print('Seu peso ideal é: %s' % resultado)

    except Exception as err:
        print('O programa será finalizado, %s' % err)


# João Papo-de- Pescador, homem de bem, comprou um microcomputador para controlar o
# rendimento diário de seu trabalho. Toda vez que ele traz um peso de peixes maior que o
# estabelecido pelo regulamento de pesca do estado de São Paulo (50 quilos) deve pagar uma
# multa de R$ 4,00 por quilo excedente. João precisa que você faça um programa que leia a
# variável peso (peso de peixes) e verifique se há excesso. Se houver, gravar na variável excesso
# e na variável multa o valor da multa que João deverá pagar. Caso contrário mostrar tais variáveis
# com o conteúdo ZERO.
def exercicio_14():
    try:
        peso_maximo_regulamento = 50
        valor_multa_por_quilo = 4.00
        peso_peixe = input('Informe o peso do peixe: ')
        if (not isinstance(peso_peixe, Number)):
            raise Exception("você precisa informar o peso do peixe em números")
        if peso_peixe > peso_maximo_regulamento:
            peso_execedente = peso_peixe - peso_maximo_regulamento
            valor_multa_ex = float(valor_multa_por_quilo) * float(peso_execedente)
        else:
            peso_execedente = 'ZERO'
            valor_multa_ex = 'ZERO'
        print('O de peso excedido foi %s' % peso_execedente)
        print('O valor da multa a ser pago é R$ %s' % valor_multa_ex)

    except Exception as err:
        print('O programa será finalizado, %s' % err)


# Faça um Programa que pergunte quanto você ganha por hora e o número de horas trabalhadas no mês. Calcule e
#  mostre o total do seu salário no referido mês, sabendo-se que são
# descontados 11% para o Imposto de Renda, 8% para o INSS e 5% para o sindicato, faça um programa que nos dê:
def exercicio_15():
    try:
        ganho_hora = input('Informe quanto você ganha por hora: ')
        if type(ganho_hora) is not float:
            raise Exception("É necessário informar a sua ganho no formato correto [ex(40.00)]")
        horas_trabalhadas = input('Informe a quantidade de horas trabalhadas: ')
        if type(horas_trabalhadas) is not int:
            raise Exception("É necessário informar a quantidade de horas trabalhadas")

        salario_bruto = int(ganho_hora * horas_trabalhadas)
        inss_descontado = salario_bruto * 0.05
        sindpd_desconto = salario_bruto * 0.05
        ir_desconto = salario_bruto * 0.11
        salario_liquido = salario_bruto - (inss_descontado + sindpd_desconto + ir_desconto)
        print('O salario bruto: R$ %s' % salario_bruto)
        print('Inss descontado: R$ %s' % inss_descontado)
        print('Sindicado descontado: R$ %s' % sindpd_desconto)
        print('Ir descontado: R$ %s' % ir_desconto)
        print('O salario liquido foi: R$ %s' % salario_liquido)

    except Exception as err:
        print('O programa será finalizado, %s' % err)


# Faça um programa para uma loja de tintas.
# O programa deverá pedir o tamanho em metros quadrados da área a ser pintada.
# Considere que a cobertura da tinta é de 1 litro para cada 3 metros quadrados e que a tinta é vendida em latas de 18 litros,
# que custam R$ 80,00. Informe ao usuário a quantidades de latas de tinta a serem compradas e o preço total.

def exercicio_16():
    try:
        metros_quadrados = input('Informe o metro quadrado a ser pintado: ')
        if type(metros_quadrados) is not int:
            raise Exception('Você deve informar o metro quadrado em números')
        metros_por_lata = float(18 * 3)
        quantidade_latas = 0
        if metros_quadrados > metros_por_lata:
            quantidade_latas = int(math.ceil(float(float(metros_quadrados) / float(metros_por_lata))))
        else:
            quantidade_latas = 1
        preco_total = float(quantidade_latas * 80.00)
        print('A quantidade de latas será: %s' % quantidade_latas)
        print('Preço Total é: R$ %s' % preco_total)
    except Exception as err:
        print('O programa será finalizado %s' % err)

#Faça um Programa para uma loja de tintas.
# O programa deverá pedir o tamanho em metros quadrados da área a ser pintada.
# Considere que a cobertura da tinta é de 1 litro para cada 6 metros quadrados e que a tinta é vendida em latas de 18 litros,
# que custam R$ 80,00 ou em galões de 3,6 litros, que custam R$ 25,00.
def exercicio_17():
    try:
        print 'Calculo para verificar quantas latas/galões de tintas serão necessarias e o valor delas'
        METROS = input("Entre com o tamanho em metros quadrados da área a ser pintada: ")
        METROSLATAS = METROS / 6
        if (METROSLATAS <= 0):
            METROSLATAS = 1

        QTDLATAS18 = math.floor(METROSLATAS / 18 + (18 * 0.10))
        QTDGALOES36 = math.floor(METROSLATAS / 3.6 + (3.6 * 0.10))
        QTDLATAS = METROSLATAS / 18
        RESTO = METROSLATAS % 18

        if (RESTO > 0 and RESTO <= 3.6):
            QTDGALOES = 1
        elif (RESTO == 0):
            QTDGALOES = 0
        else:
            QTDGALOES = math.floor(RESTO / 3.6 + (3.6 * 0.10))

        if (QTDLATAS18 <= 0 or QTDGALOES36 <= 0 or QTDGALOES < 0):
            QTDGALOES36 = 1
            QTDLATAS18 = 1
            QTDGALOES = 1

        PRECOLATAS18 = QTDLATAS18 * 80
        PRECOGALOES36 = QTDGALOES36 * 25
        PRECOLATAS = QTDLATAS * 80
        PRECOGALOES = QTDGALOES * 25

        PRECOTIMO = PRECOLATAS + PRECOGALOES

        print (
              '\n Quantidade de latas: %d latas. Preço latas: %.2f reais. \n Quantidades galões: %d galões. Preço galões: %.2f. \n Solução Otima, latas: %d e galões: %d Valor:%.2f') % (
              QTDLATAS18, PRECOLATAS18, QTDGALOES36, PRECOGALOES36, QTDLATAS, QTDGALOES, PRECOTIMO)

    except Exception as err:
        print('O programa será finalizado %s' % err)

def exercicio_18():
    # Programa para calcular velocidade de download
    arquivo = input('Informe o tamanho do arquivo para donwload (em MB): ')
    print
    velocidade = input('Informe a velocidade de sua internet (em MBPS): ')
    tempo = arquivo / velocidade
    minuto = tempo / 60.0
    print
    print 'O tempo aproximado para download do arquivo usando este link e de: %.2f minutos' % minuto

