# coding: utf-8
# !/usr/bin/python
import random
import sys
from datetime import date


# Faça um programa, com uma função que necessite de três argumentos,
# e que forneça a soma desses três argumentos
def exercicio_1(*args):
    try:
        lst_args = []
        for item in args:
            if type(item) is not int or type(item) is not int:
                raise Exception('Você só pode fornecer argumentos do tipo númerico')
            else:
                lst_args.append(item)
        print('Argumentos enviados')
        print(lst_args)
        print('Soma dos argumentos enviados: %s' % sum(lst_args))

    except Exception as err:
        print('erro: %s' % err)


# Faça um programa, com uma função que necessite de um argumento.
# A função retorna o valor de caractere ‘P’, se seu argumento for positivo, e ‘N’,
# se seu argumento for zero ou negativo.
def exercicio_2(arg):
    try:
        if type(arg) is not int or type(arg) is not int:
            raise Exception('Você só pode fornecer argumentos do tipo númerico')
        else:
            if arg > 0:
                print('Argumento Enviado é [P]')
            else:
                print('Argumento Enviado é [N]')
    except Exception as err:
        print('erro: %s' % err)


# Faça um programa com uma função chamada somaImposto.
# A função possui dois parâmetros formais: taxaImposto,
# que é a quantia de imposto sobre vendas expressa em porcentagem e custo,
# que é o custo de um item antes do imposto. A função “altera” o valor de custo para incluir o imposto sobre vendas.
def somaImposto(taxaImposto, custo):
    try:
        if type(taxaImposto) is not int and type(taxaImposto) is not float:
            raise Exception('Você só pode fornecer taxaImposto do tipo númerico')
        elif type(custo) is not int and type(custo) is not float:
            raise Exception('Você só pode fornecer custo do tipo númerico')
        imposto = float(float(taxaImposto) / 100)
        custo += custo * imposto
        print('O custo do produto com o imposto é R $s' % custo)

    except Exception as err:
        print('erro: %s' % err)


# """Faça um programa para imprimir:
#             1
#             2    2
#             3    3   3
#             .....
#             n    n   n   n    n    n   ... n
#      para um n informado pelo usuário. Use uma função que receba um valor n inteiro e imprima até a n-ésima linha.
# """
def exercicio_4(valor):
    try:
        if isinstance(valor, int):
            x = 1
            while x <= valor:
                y = 1
                texto = ''
                while y <= x:
                    texto += str(x) + "\t"
                    y += 1
                print texto
                x += 1
        else:
            raise Exception('Informe apenas números inteiros')
    except Exception as err:
        print('erro: %s' % err)


# Faça um programa para imprimir:
#     1
#     1   2
#     1   2   3
#     .....
#     1   2   3   ...  n
# para um n informado pelo usuário. Use uma função que receba um valor ninteiro imprima até a n-ésima linha.
def exercicio_5(valor):
    try:
        texto = ''
        if isinstance(valor, int):
            x = 1
            while x <= valor:
                texto += str(x) + "\t"
                print(texto)
                x += 1
        else:
            raise Exception('Informe apenas números inteiros')
    except Exception as err:
        print('erro: %s' % err)


# Reverso do número. Faça uma função que retorne o reverso de um número inteiro informado.
# Por exemplo: 127 -> 721.
def exercicio_6(valor):
    try:
        if isinstance(valor, int):
            numero_invertido = int(str(valor)[::-1])
            print('O número informado foi %s' % valor)
            print('Número invertido %s' % numero_invertido)
        else:
            raise Exception('Informe apenas números inteiros')
    except Exception as err:
        print('Erro: %s' % err)


# Jogo de Craps. Faca um programa de implemente um jogo de Craps. O jogador
# lanca um par de dados, obtendo um valor entre 2 e 12. Se, na primeira jogada,
# voce tirar 7 ou 11, voce tirou um "natural" e ganhou. Se voce tirar 2, 3 ou
# 12 na primeira jogada, isto e chamado de "craps" e voce perdeu. Se, na
# primeira jogada, voce fez um 4, 5, 6, 8, 9 ou 10,este e seu "Ponto". Seu
# objetivo agora e continuar jogando os dados ate tirar este numero novamente.
# Voce perde, no entanto, se tirar um 7 antes de tirar este Ponto novamente.
# Dica: para simular o lancamento do dado, utilize o metodos Random do Python.!
def lancar_dados():
    return random.randint(2, 12)


def exercicio_7():
    try:
        entrada = ""
        jogada = 0
        ponto = 0
        print "digite  \"sair\" para sair (sem aspas)\naperte <enter> para rolar os dados: "

        while (entrada != "sair"):
            jogada += 1
            print ("Jogada {}".format(jogada))
            entrada = raw_input("Esperando acao: ")
            if entrada == "sair":
                print "Saindo do jogo... Tchau"
            else:
                if jogada > 1:
                    print ("Seu ponto e {}".format(ponto))
                valor = lancar_dados()
                print ("O valor do dado e {}\n\n".format(valor))
                if jogada == 1:
                    if valor == 7 or valor == 11:
                        print ("Voce tirou um natural e ganhou, Parabéns")
                        exit()
                    elif valor == 2 or valor == 3 or valor == 12:
                        print ("Voce tirou um craps e perdeu, melhor sorte da proxima vez")
                        exit()
                    else:
                        ponto = valor
                else:
                    if valor == 7:
                        print ("Voce tirou um 7 antes de repetir seu ponto, voce perdeu")
                        exit()
                    elif ponto == valor:
                        print ("Voce conseguiu repetir seu ponto e ganhou, Parabéns")
                        exit()
    except Exception as err:
        print('Erro: %s' % err)


# Construa uma função que receba uma string como parâmetro e devolva outra string com os carateres embaralhados.
# Por exemplo: se função receber a palavra python, pode retornar npthyo, ophtyn ou qualquer outra combinação possível,
# de forma aleatória. Padronize em sua função que todos os caracteres serão devolvidos em caixa alta ou caixa baixa,
# independentemente de como foram digitados.

def exercicio_8(string, caixa=None):
    try:
        if isinstance(string, str):
            lista = []
            for i in range(0, len(string)):
                lista.append(string[i])
            random.shuffle(lista)
            saida = ''
            print (saida)
            for c in lista:
                if caixa == 'upper':
                    saida += str(c.upper())
                elif caixa == 'lower':
                    saida += str(c.lower())
                else:
                    saida += str(c)

            print(saida)
        else:
            raise Exception('É permitido enviar apenas strings')
    except Exception as err:
        print('erro: %s' % err)


# Data com mês por extenso. Construa uma função que receba uma data no formato DD/MM/AAAA e devolva uma string
# no formato D de mesPorExtenso de AAAA. Opcionalmente, valide a data e retorne NULL caso a data seja inválida.
def exercicio_9():
    try:
        data = raw_input("digite uma data com o seguinte formato dd/mm/aaaa ---> ")
        dia = int(data[0:2])
        mes = int(data[3:5])
        ano = int(data[6:10])
        validade = "true"
        i = 0
        while validade == "true" and i == 0:
            if (ano % 4 == 0 and ano % 100 != 0) or ano % 400 == 0:
                bissexto = "sim"
            else:
                bissexto = "nao"

            if mes < 1 or mes > 12:
                validade = "false"

            if dia > 31 or ((mes == 4 or mes == 6 or mes == 9 or mes == 11) and dia > 30):
                validade = "false"

            if (mes == 2 and bissexto == "nao" and dia > 28) or (mes == 2 and bissexto == "sim" and dia > 29):
                validade = "false"
            i = i + 1

        if validade == "true":
            lista_de_mes = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro",
                            "Outubro", "Novembro", "Dezembro"]
            mes_ex = lista_de_mes[mes - 1]
            print('%s de %s de %s' % (dia, mes_ex, ano))
        else:
            raise Exception('Data Invalida')
    except Exception as err:
        print('Erro: %s' % err)


# Faça uma função que informe a quantidade de dígitos de um determinado número inteiro informado.
def exercicio_10():
    try:
        n_1 = input("Informe um número inteiro: ")
        if isinstance(n_1, int):
           print('Quantidade de digitos informados é %s' % len(str(n_1)))
        else:
            print('O valor informado não é um número inteiro')

    except Exception as err:
        print('erro %s' % err)

# exercicio_1(1, 2, 10)
# exercicio_2(1)
# somaImposto(1.5, 200)
# exercicio_4(10)
# exercicio_5(10)
# exercicio_6(127)
# exercicio_7()
# exercicio_8('Leandro','upper')
# exercicio_9()
# exercicio_10()
