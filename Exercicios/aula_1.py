
# coding: utf-8

# In[2]:

#subtração
2-1


# In[1]:

#soma
2+2


# In[4]:

#divisao
4/2


# Types

# In[9]:

type(3.6)


# In[10]:

type(3)


# In[11]:

type(207-10-10)


# In[12]:

a = 'Leandro Santana'
type(a)


# In[14]:

type(int(6.0))


# In[15]:

int(6.9)


# In[16]:

bin(390)

Outras funções
# In[17]:

round(1.356565,2)


# In[18]:

pow(4,2)

