import sqlite3
print('-----------------------------------------------------------')
con = sqlite3.connect('escola.db')
cur = con.cursor();

sql_delete = 'DELETE FROM cursos'
cur.execute(sql_delete)

sql_create = 'CREATE TABLE IF NOT EXISTS cursos ' \
             '(id integer  primary key, ' \
             'titulo varchar(100), ' \
             'categoria varchar(140))'
cur.execute(sql_create)
sql_insert = 'INSERT INTO cursos VALUES (?,?,?)'
rec_set = [(1000, 'Ciência de Dados', 'Data Science'),
           (1001, 'Big Data Fundamentos', 'Big Data'),
           (1002, 'Python Fundamento', 'Análise de Dados')]

for rec in rec_set:
   cur.execute(sql_insert, rec);

con.commit()

sql_select = 'SELECT * FROM cursos'
cur.execute(sql_select)
dados =  cur.fetchall()
for linha in dados:
    print('Curso Id: %d, Título: %s, Categoria: %s \n' % linha)


rec_set = [(1003, 'Gestão de Dados MongoDB', 'Big Data'),
           (1004, 'R Fundamentos ', 'Analise de Dados')]


for rec in rec_set:
    cur.execute(sql_insert,rec)
con.commit()
print('-----------------------------------------------------------')

cur.execute(sql_select)
rec_set = cur.fetchall()
for rec in rec_set:
    print('Curso Id: %d, Titulo: %s, Categoria %s \n' % rec)

con.close()

