import sqlite3

conn = sqlite3.connect('dsa.db')
c = conn.cursor()


# Função para criar tabela
def create_table():
    c.execute('CREATE TABLE IF NOT EXISTS produtos(' \
              'id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,' \
              'date TEXT,' \
              'prod_name TEXT, valor REAL)')


# Função para inserir uma linha
def data_insert():
    c.execute("INSERT INTO produtos VALUES(10,'2016-05-02 14:32:11','Teclado',90)")
    conn.commit()
    c.close()
    conn.close()


create_table()
data_insert()


