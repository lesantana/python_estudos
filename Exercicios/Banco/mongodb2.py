# coding: utf-8
import pymongo

cliente_con = pymongo.MongoClient()

databases = cliente_con.database_names()

# print(databases)
db = cliente_con.cadastrodb

collections = db.collection_names()

# db.create_collection('mycollection')
# print(collections)
# db.mycollection.insert_one({
#     'titulo':'MongoDb com Python',
#     'descricao':'MongoDB é um Banco de dados NoSql',
#     'by':'Data Science Academy',
#     'url':'www.datascienceacademy.com.br',
#     'tags':['mongodb','database','Nosql'],
#     'likes':100
# })

# result = db.mycollection.find_one()
# print(result)
# doc1 = {'Nome':'Barack','Sobrenome':'Obama','twitter':'@barack'}
# db.mycollection.insert_one(doc1)

# doc2 = {"Site":'http://www.datascienceacademy.com.br','facebook':'facebook.com/dsacademybr'}
# db.mycollection.insert_one(doc2)

for rec in db.mycollection.find():
    print(rec)

col = db['mycollection']

# print(type(db)
# print(col.count())

# reddoc = col.find_one()
# print(reddoc)
