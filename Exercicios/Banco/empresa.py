# coding: utf-8
import sqlite3
import random
import time
import datetime
import matplotlib.pyplot as plt

# %matplotlib notebook

conn = sqlite3.connect('dsa.db')
c = conn.cursor()


# Função para criar Tabela
def create_table():
    c.execute('CREATE TABLE IF NOT EXISTS produtos(' \
              'id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,' \
              'date TEXT,' \
              'prod_name TEXT, valor REAL)')


# Função para inserir uma linha
def data_insert():
    c.execute("INSERT INTO produtos VALUES(10,'2016-05-02 14:32:11','Teclado',90)")
    conn.commit()
    c.close()
    conn.close()


def data_insert_var():
    new_date = datetime.datetime.now()
    new_prod_name = 'Monitor'
    new_valor = random.randrange(50, 100)
    c.execute('INSERT INTO produtos (date,prod_name,valor) VALUES (?,?,?)', (new_date, new_prod_name, new_valor))
    conn.commit()


def leitura_todos_dados():
    c.execute('SELECT * FROM produtos')
    for linha in c.fetchall():
        print(linha)


def leitura_registros():
    c.execute('SELECT * FROM produtos WHERE valor > 92')
    for linha in c.fetchall():
        print(linha)


def leitura_colunas():
    c.execute('SELECT * FROM produtos LIMIT 2,5')
    for linha in c.fetchall():
        print(linha[3])


def atualiza_dados():
    c.execute("UPDATE produtos SET prod_name = 'Televisor' WHERE prod_name = 'Teclado'")
    conn.commit()


def remove_dados():
    c.execute('DELETE FROM produtos WHERE valor > 50')
    conn.commit()


def dados_grafico():
    c.execute('SELECT id, valor FROM produtos')
    ids = []
    valores = []
    dados = c.fetchall()
    for linha in dados:
        ids.append(linha[0])
        valores.append(linha[1])

    plt.bar(ids, valores)
    plt.show()

data_insert_var()
dados_grafico()


# for i in range(10):
#     data_insert_var()
#     time.sleep(1)
# c.close()
# conn.close()
